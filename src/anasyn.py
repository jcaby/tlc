#!/usr/bin/python

# @package anasyn
# 	Syntactical Analyser package.
#

import sys
import argparse
import re
import logging

import analex

logger = logging.getLogger('anasyn')

DEBUG = False
LOGGING_LEVEL = logging.DEBUG

class AnaSynException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class TableIdentificateurs(object):

    def __init__(self):
        self.array=[]
    
    def affecter_type(self, type_variable):
        i = -1
        # parcours en partant du dernier
        # boucler tant qu'on trouve des types pas encore définis
        while i >= -len(self.array) and self.array[i][2] == 'None':
            self.array[i][2] = type_variable
            i = i - 1

    def affecter_mode(self, mode):
        i = -1
        # parcours en partant du dernier
        # boucler tant qu'on trouve des types pas encore définis
        while i >= -len(self.array) and self.array[i][4] == 'None':
            self.array[i][4] = mode
            tableProcedures.setMode(mode, codeGenerator.stackOfContexts[-2])
            i = i - 1  

    def ajouter(self, ident):
        self.array.append([ident, -1, "None", codeGenerator.getContext()]) # nom, adresse statique, type ,(in / in out) si défini pour une fonction /"variable" sinon [, fonction associée] 
        if(codeGenerator.getContext() == 'param'): #si l'élément qu'on ajoute est un paramètre
            self.array[-1].append("None") #sera changer en in ou in out
            self.array[-1].append(codeGenerator.stackOfContexts[-2])
            codeGenerator.incrementerNbParam()
        else: #l'élément qu'on ajoute est une variable
            self.array[-1].append("Variable")
            codeGenerator.incrementerNbVariable()
        self.array[-1][1] = codeGenerator.getNbVariables() + codeGenerator.getNbParam() -1

    def retirer(self):
        context = codeGenerator.getContext()
        for i in reversed(range(len(self.array))):
            if(self.array[i][3] == context or self.array[i][3] == 'param'):
                self.array.pop(i)
        codeGenerator.resetNbParam()

class CodeGenerator(object):

    def __init__(self):
        self.arrayOfinstructions = ['debutProg'] # initialiser avec debutProg toujours
        self.nbVariables = 0 # nombre de variables
        self.nbParam = 0 # nombre de paramètres dans un contexte de fonction
        self.stackOfContexts = [] # pile des contexte
        self.storedInstructions = [] # pile d'instructions à ajouter dans le code objet après avoir empiler leur termes

        self.nbInstanciatedParams = -1
        self.identOfCalledProc = ""
    
    #fonction servant à ajouter une instruction
    def addInstruction(self, instr):
        self.arrayOfinstructions.append(instr)
    
    #fonctions permettant de savoir dans quel context on se trouve
    def addContext(self, string):
        self.stackOfContexts.append(string)
    
    def popContext(self):
        self.stackOfContexts.pop()
    
    def getContext(self):
        if(self.stackOfContexts):
            return self.stackOfContexts[-1]
        else:
            return 'main'

    #fonction qui empile l'adresse d'une variable à partir de son nom
    def addEmpilerVariable(self,var):
        trouve = False
        iterator = -1
        while trouve == False and iterator >= -len(tableIdentificateurs.array):
            i = tableIdentificateurs.array[iterator]
            if(i[0] == var):
                trouve = True
            iterator -= 1
        if(trouve == True):
            if(i[3] == 'main'): #variable globale
                strToAdd = 'empiler(' + str(i[1]) + ')'
            elif(i[4] == 'in out'):
                strToAdd = 'empilerParam(' + str(i[1]) + ')'
            else: #variable locale ou paramètre in
                strToAdd = 'empilerAd(' + str(i[1]) + ')'
            self.addInstruction(strToAdd)
        else:
            print("Erreur: variable introuvable")

    def get_instruction_counter(self):
        return len(self.arrayOfinstructions)

    def get_instruction_at_index(self, instrIndex):
        return self.arrayOfinstructions[instrIndex]

    #retourne le nombre d'instructions déjà écrites
    def getNbOfInstructions(self):
        return len(self.arrayOfinstructions)

    #fonctions utilisées pour reserver le bon nombre d'emplacements en mémoire
    def incrementerNbVariable(self):
        self.nbVariables = self.nbVariables + 1

    def getNbVariables(self):
        return self.nbVariables
    
    def incrementerNbParam(self):
        self.nbParam = self.nbParam + 1

    def getNbParam(self):
        return self.nbParam
    
    def resetNbParam(self):
        self.nbParam = 0

    def addReserver(self):
        strToAdd = 'reserver(' + str(self.nbVariables) + ')'
        self.addInstruction(strToAdd)
        self.nbVariables = 0
    
    #fonctions utilisées pour ajouter les opérations et comparaisons au bon endroit (add(), egal(), ...)
    def addOu(self):
        strToAdd = 'ou()'
        self.addInstruction(strToAdd)
    
    def addEt(self):
        strToAdd = 'et()'
        self.addInstruction(strToAdd) 
    
    def storeInstruction(self, instruction):
        # print("--- storing "+instruction)
        self.storedInstructions.append(instruction)
    
    def addStoredInstruction(self):
        #rien à faire si  l'instruction stockée est 'plus()' pour OpUnaire
        instruction = self.storedInstructions.pop()
        if(instruction == "plus()"):
            return
        # print("--- adding "+instruction)
        self.addInstruction(instruction)

    #fonctions utilisées pour la gestion des while et if
    def addTze(self, adresse = '...'):
        strToAdd = 'tze('+str(adresse)+')'
        self.addInstruction(strToAdd)

    def addTra(self, adresse ='...'):
        strToAdd = 'tra('+str(adresse)+')'
        self.addInstruction(strToAdd)
    
    #fonctions pour les fonctions et procédures
    def completeTra(self):
        index = self.arrayOfinstructions.index("saut pour les procédures ici")
        self.arrayOfinstructions[index] = 'tra(' + str(len(self.arrayOfinstructions)+1) + ')'
    
    def addTraStat(self, ident):
        strToAdd = 'traStat(' + str(tableProcedures.getStartingAdr(ident)) + ',' + str(tableProcedures.getNbParam(ident)) + ')'
        
        self.addInstruction(strToAdd)


class TableProcedures(object):

    def __init__(self):
        self.dico={} # clef: nom des procédures; valeurs: informations concernant les procédures
    
    def ajouter(self, ident):
        self.dico[ident] = [codeGenerator.getNbOfInstructions()+1, -1, []] #adresse de retour, nb de paramètres, liste des modes de chaque paramètre (in ou in out)
    
    #ajoute un mode à la liste des modes d'une procédure
    def setMode(self,mode, ident):
        self.dico[ident][-1].append(mode)
    
    def getStartingAdr(self, ident):
        return self.dico[ident][0]
    
    def setNbParam(self):
        context = codeGenerator.stackOfContexts[-2]
        self.dico[context][1]= codeGenerator.getNbParam()
    
    def getNbParam(self, ident):
        return self.dico[ident][1]


tableIdentificateurs = TableIdentificateurs()
codeGenerator = CodeGenerator()
tableProcedures = TableProcedures()

########################################################################
# Syntactical Diagrams
########################################################################


def program(lexical_analyser):
    specifProgPrinc(lexical_analyser)
    lexical_analyser.acceptKeyword("is")
    corpsProgPrinc(lexical_analyser)


def specifProgPrinc(lexical_analyser):
    lexical_analyser.acceptKeyword("procedure")
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("Name of program : "+ident)


def corpsProgPrinc(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        logger.debug("Parsing declarations")
        partieDecla(lexical_analyser)
        logger.debug("End of declarations")
    lexical_analyser.acceptKeyword("begin")

    if not lexical_analyser.isKeyword("end"):
        logger.debug("Parsing instructions")
        suiteInstr(lexical_analyser)
        logger.debug("End of instructions")

    lexical_analyser.acceptKeyword("end")
    lexical_analyser.acceptFel()
    codeGenerator.addInstruction("finProg")
    logger.debug("End of program")


def partieDecla(lexical_analyser):
    if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function"):
        codeGenerator.addInstruction("saut pour les procédures ici")
        listeDeclaOp(lexical_analyser)
        codeGenerator.completeTra()
        if not lexical_analyser.isKeyword("begin"):
            listeDeclaVar(lexical_analyser)

    else:
        listeDeclaVar(lexical_analyser)
    codeGenerator.addReserver()


def listeDeclaOp(lexical_analyser):
    declaOp(lexical_analyser)
    lexical_analyser.acceptCharacter(";")
    if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function"):
        listeDeclaOp(lexical_analyser)


def declaOp(lexical_analyser):
    if lexical_analyser.isKeyword("procedure"):
        procedure(lexical_analyser)
    if lexical_analyser.isKeyword("function"):
        fonction(lexical_analyser)


def procedure(lexical_analyser):
    lexical_analyser.acceptKeyword("procedure")
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("Name of procedure : "+ident)
    codeGenerator.addContext(ident)
    tableProcedures.ajouter(ident)
    partieFormelle(lexical_analyser)
    lexical_analyser.acceptKeyword("is")
    corpsProc(lexical_analyser)


def fonction(lexical_analyser):
    lexical_analyser.acceptKeyword("function")
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("Name of function : "+ident)
    codeGenerator.addContext(ident)
    tableProcedures.ajouter(ident)
    partieFormelle(lexical_analyser)

    lexical_analyser.acceptKeyword("return")
    nnpType(lexical_analyser)

    lexical_analyser.acceptKeyword("is")
    corpsFonct(lexical_analyser)


def corpsProc(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        partieDeclaProc(lexical_analyser)
    lexical_analyser.acceptKeyword("begin")
    suiteInstr(lexical_analyser)
    lexical_analyser.acceptKeyword("end")
    codeGenerator.addInstruction("retourProc()")
    tableIdentificateurs.retirer()
    codeGenerator.popContext()


def corpsFonct(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        partieDeclaProc(lexical_analyser)
    lexical_analyser.acceptKeyword("begin")
    suiteInstrNonVide(lexical_analyser)
    lexical_analyser.acceptKeyword("end")
    tableIdentificateurs.retirer()
    codeGenerator.popContext()


def partieFormelle(lexical_analyser):
    lexical_analyser.acceptCharacter("(")
    codeGenerator.addContext('param')
    if not lexical_analyser.isCharacter(")"):
        listeSpecifFormelles(lexical_analyser)
    tableProcedures.setNbParam()
    codeGenerator.popContext()
    lexical_analyser.acceptCharacter(")")


def listeSpecifFormelles(lexical_analyser):
    specif(lexical_analyser)
    if not lexical_analyser.isCharacter(")"):
        lexical_analyser.acceptCharacter(";")
        listeSpecifFormelles(lexical_analyser)


def specif(lexical_analyser):
    listeIdent(lexical_analyser)
    lexical_analyser.acceptCharacter(":")
    if lexical_analyser.isKeyword("in"):
        mode(lexical_analyser)
    else:
        tableIdentificateurs.affecter_mode('in')
    nnpType(lexical_analyser)


def mode(lexical_analyser):
    lexical_analyser.acceptKeyword("in")
    if lexical_analyser.isKeyword("out"):
        lexical_analyser.acceptKeyword("out")
        logger.debug("in out parameter")
        mode = 'in out'
    else:
        logger.debug("in parameter")
        mode = 'in'
    tableIdentificateurs.affecter_mode(mode)


def nnpType(lexical_analyser):
    if lexical_analyser.isKeyword("integer"):
        lexical_analyser.acceptKeyword("integer")
        logger.debug("integer type")
        tableIdentificateurs.affecter_type("integer")
    elif lexical_analyser.isKeyword("boolean"):
        lexical_analyser.acceptKeyword("boolean")
        logger.debug("boolean type")
        tableIdentificateurs.affecter_type("boolean")
    else:
        logger.error("Unknown type found <" +
                     lexical_analyser.get_value() + ">!")
        raise AnaSynException("Unknown type found <" +
                              lexical_analyser.get_value() + ">!")

def partieDeclaProc(lexical_analyser):
    listeDeclaVar(lexical_analyser)
    codeGenerator.addReserver()


def listeDeclaVar(lexical_analyser):
    declaVar(lexical_analyser)
    if lexical_analyser.isIdentifier():
        listeDeclaVar(lexical_analyser)
    


def declaVar(lexical_analyser):
    listeIdent(lexical_analyser)
    lexical_analyser.acceptCharacter(":")
    logger.debug("now parsing type...")
    nnpType(lexical_analyser)
    lexical_analyser.acceptCharacter(";")


def listeIdent(lexical_analyser):
    ident = lexical_analyser.acceptIdentifier()
    tableIdentificateurs.ajouter(ident)
    logger.debug("identifier found: "+str(ident))

    if lexical_analyser.isCharacter(","):
        lexical_analyser.acceptCharacter(",")
        listeIdent(lexical_analyser)


def suiteInstrNonVide(lexical_analyser):
    instr(lexical_analyser)
    if lexical_analyser.isCharacter(";"):
        lexical_analyser.acceptCharacter(";")
        suiteInstrNonVide(lexical_analyser)


def suiteInstr(lexical_analyser):
    if not lexical_analyser.isKeyword("end"):
        suiteInstrNonVide(lexical_analyser)


def instr(lexical_analyser):
    if lexical_analyser.isKeyword("while"):
        boucle(lexical_analyser)
    elif lexical_analyser.isKeyword("if"):
        altern(lexical_analyser)
    elif lexical_analyser.isKeyword("get") or lexical_analyser.isKeyword("put"):
        es(lexical_analyser)
    elif lexical_analyser.isKeyword("return"):
        retour(lexical_analyser)
    elif lexical_analyser.isIdentifier():
        ident = lexical_analyser.acceptIdentifier()
        if lexical_analyser.isSymbol(":="):
            # affectation
            codeGenerator.addEmpilerVariable(ident)
            lexical_analyser.acceptSymbol(":=")
            expression(lexical_analyser)
            codeGenerator.addInstruction("affectation()")
            logger.debug("parsed affectation")
        elif lexical_analyser.isCharacter("("): #ident désigne une procédure à lancer   
            codeGenerator.addInstruction("reserverBloc()")
            lexical_analyser.acceptCharacter("(")
            if not lexical_analyser.isCharacter(")"):
                codeGenerator.identOfCalledProc = ident
                codeGenerator.nbInstanciatedParams = 0
                listePe(lexical_analyser)
                codeGenerator.identOfCalledProc = ""
                codeGenerator.nbInstanciatedParams = -1
            lexical_analyser.acceptCharacter(")")
            codeGenerator.addTraStat(ident)
            logger.debug("parsed procedure call")
        else:
            logger.error("Expecting procedure call or affectation!")
            raise AnaSynException("Expecting procedure call or affectation!")

    else:
        logger.error("Unknown Instruction <" +
                     lexical_analyser.get_value() + ">!")
        raise AnaSynException("Unknown Instruction <" +
                              lexical_analyser.get_value() + ">!")


def listePe(lexical_analyser):
    expression(lexical_analyser)
    codeGenerator.nbInstanciatedParams = codeGenerator.nbInstanciatedParams + 1
    if lexical_analyser.isCharacter(","):
        lexical_analyser.acceptCharacter(",")
        listePe(lexical_analyser)


def expression(lexical_analyser):
    logger.debug("parsing expression: " + str(lexical_analyser.get_value()))

    exp1(lexical_analyser)
    if lexical_analyser.isKeyword("or"):
        lexical_analyser.acceptKeyword("or")
        expression(lexical_analyser) # exp1() remplacé
        codeGenerator.addOu()


def exp1(lexical_analyser):
    logger.debug("parsing exp1")

    exp2(lexical_analyser)
    if lexical_analyser.isKeyword("and"):
        lexical_analyser.acceptKeyword("and")
        exp1(lexical_analyser) # exp2() remplacé
        codeGenerator.addEt()


def exp2(lexical_analyser):
    logger.debug("parsing exp2")

    exp3(lexical_analyser)
    if lexical_analyser.isSymbol("<") or \
            lexical_analyser.isSymbol("<=") or \
            lexical_analyser.isSymbol(">") or \
            lexical_analyser.isSymbol(">="):
        opRel(lexical_analyser)
        exp2(lexical_analyser) # exp3() remplacé
        codeGenerator.addStoredInstruction()
    elif lexical_analyser.isSymbol("=") or \
            lexical_analyser.isSymbol("/="):
        opRel(lexical_analyser)
        exp2(lexical_analyser) # exp3() remplacé
        codeGenerator.addStoredInstruction()


def opRel(lexical_analyser):
    logger.debug("parsing relationnal operator: " +
                 lexical_analyser.get_value())

    if lexical_analyser.isSymbol("<"):
        lexical_analyser.acceptSymbol("<")
        codeGenerator.storeInstruction("inf()")

    elif lexical_analyser.isSymbol("<="):
        lexical_analyser.acceptSymbol("<=")
        codeGenerator.storeInstruction("infeg()")

    elif lexical_analyser.isSymbol(">"):
        lexical_analyser.acceptSymbol(">")
        codeGenerator.storeInstruction("sup()")

    elif lexical_analyser.isSymbol(">="):
        lexical_analyser.acceptSymbol(">=")
        codeGenerator.storeInstruction("supeg()")

    elif lexical_analyser.isSymbol("="):
        lexical_analyser.acceptSymbol("=")
        codeGenerator.storeInstruction("egal()")

    elif lexical_analyser.isSymbol("/="):
        lexical_analyser.acceptSymbol("/=")
        codeGenerator.storeInstruction("diff()")

    else:
        msg = "Unknown relationnal operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def exp3(lexical_analyser):
    logger.debug("parsing exp3")
    exp4(lexical_analyser)
    if lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-"):
        opAdd(lexical_analyser)
        exp3(lexical_analyser) # exp4() remplacé
        codeGenerator.addStoredInstruction()


def opAdd(lexical_analyser):
    logger.debug("parsing additive operator: " + lexical_analyser.get_value())
    if lexical_analyser.isCharacter("+"):
        lexical_analyser.acceptCharacter("+")
        codeGenerator.storeInstruction("add()")

    elif lexical_analyser.isCharacter("-"):
        lexical_analyser.acceptCharacter("-")
        codeGenerator.storeInstruction("sous()")

    else:
        msg = "Unknown additive operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def exp4(lexical_analyser):
    logger.debug("parsing exp4")

    prim(lexical_analyser)
    if lexical_analyser.isCharacter("*") or lexical_analyser.isCharacter("/"):
        opMult(lexical_analyser)
        exp4(lexical_analyser) # prim() remplacé
        codeGenerator.addStoredInstruction()


def opMult(lexical_analyser):
    logger.debug("parsing multiplicative operator: " +
                 lexical_analyser.get_value())
    if lexical_analyser.isCharacter("*"):
        lexical_analyser.acceptCharacter("*")
        codeGenerator.storeInstruction("mult()")

    elif lexical_analyser.isCharacter("/"):
        lexical_analyser.acceptCharacter("/")
        codeGenerator.storeInstruction("div()")

    else:
        msg = "Unknown multiplicative operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def prim(lexical_analyser):
    logger.debug("parsing prim")

    if lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-") or lexical_analyser.isKeyword("not"):
        opUnaire(lexical_analyser)
        elemPrim(lexical_analyser)
        codeGenerator.addStoredInstruction()
    else:
        elemPrim(lexical_analyser)


def opUnaire(lexical_analyser):
    logger.debug("parsing unary operator: " + lexical_analyser.get_value())
    if lexical_analyser.isCharacter("+"):
        lexical_analyser.acceptCharacter("+")
        codeGenerator.storeInstruction("plus()")

    elif lexical_analyser.isCharacter("-"):
        lexical_analyser.acceptCharacter("-")
        codeGenerator.storeInstruction("moins()")

    elif lexical_analyser.isKeyword("not"):
        lexical_analyser.acceptKeyword("not")
        codeGenerator.storeInstruction("non()")

    else:
        msg = "Unknown additive operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def elemPrim(lexical_analyser):
    logger.debug("parsing elemPrim: " + str(lexical_analyser.get_value()))
    if lexical_analyser.isCharacter("("):
        lexical_analyser.acceptCharacter("(")
        expression(lexical_analyser)
        lexical_analyser.acceptCharacter(")")
    elif lexical_analyser.isInteger() or lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
        valeur(lexical_analyser)
    elif lexical_analyser.isIdentifier():
        ident = lexical_analyser.acceptIdentifier()
        if lexical_analyser.isCharacter("("):   # Appel fonct
            codeGenerator.addInstruction("reserverBloc()")
            lexical_analyser.acceptCharacter("(")
            if not lexical_analyser.isCharacter(")"):
                codeGenerator.identOfCalledProc = ident
                codeGenerator.nbInstanciatedParams = 0
                listePe(lexical_analyser)
                codeGenerator.identOfCalledProc = ""
                codeGenerator.nbInstanciatedParams = -1
            lexical_analyser.acceptCharacter(")")
            codeGenerator.addTraStat(ident)
            logger.debug("parsed procedure call")

            logger.debug("Call to function: " + ident)
        else:
            logger.debug("Use of an identifier as an expression: " + ident)
            codeGenerator.addEmpilerVariable(ident)
            if(codeGenerator.nbInstanciatedParams != -1):
                calledProc = codeGenerator.identOfCalledProc
                nbInstanciatedParams = codeGenerator.nbInstanciatedParams
                mode = tableProcedures.dico[calledProc][2][nbInstanciatedParams]
                if mode == 'in':
                    codeGenerator.addInstruction("valeurPile()")
            else:
                codeGenerator.addInstruction("valeurPile()")
            
            

    else:
        logger.error("Unknown Value!")
        raise AnaSynException("Unknown Value!")


def valeur(lexical_analyser):
    if lexical_analyser.isInteger():
        entier = lexical_analyser.acceptInteger()
        logger.debug("integer value: " + str(entier))
        codeGenerator.addInstruction("empiler(" + str(entier) + ")")
        return "integer"
    elif lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
        vtype = valBool(lexical_analyser)
        return vtype
    else:
        logger.error("Unknown Value! Expecting an integer or a boolean value!")
        raise AnaSynException(
            "Unknown Value ! Expecting an integer or a boolean value!")


def valBool(lexical_analyser):
    if lexical_analyser.isKeyword("true"):
        lexical_analyser.acceptKeyword("true")
        codeGenerator.addInstruction("empiler(1)")
        logger.debug("boolean true value")

    else:
        logger.debug("boolean false value")
        lexical_analyser.acceptKeyword("false")
        codeGenerator.addInstruction("empiler(0)")

    return "boolean"


def es(lexical_analyser):
    logger.debug("parsing E/S instruction: " + lexical_analyser.get_value())
    if lexical_analyser.isKeyword("get"):
        lexical_analyser.acceptKeyword("get")
        lexical_analyser.acceptCharacter("(")
        ident = lexical_analyser.acceptIdentifier()
        codeGenerator.addEmpilerVariable(ident)
        lexical_analyser.acceptCharacter(")")
        codeGenerator.addInstruction("get()")
        logger.debug("Call to get "+ident)
    elif lexical_analyser.isKeyword("put"):
        lexical_analyser.acceptKeyword("put")
        lexical_analyser.acceptCharacter("(")
        expression(lexical_analyser)
        lexical_analyser.acceptCharacter(")")
        codeGenerator.addInstruction("put()")
        logger.debug("Call to put")
    else:
        logger.error("Unknown E/S instruction!")
        raise AnaSynException("Unknown E/S instruction!")


def boucle(lexical_analyser):
    emptyTze = []

    logger.debug("parsing while loop: ")
    lexical_analyser.acceptKeyword("while")

    adresseDebutBoucle = codeGenerator.getNbOfInstructions()+1
    
    expression(lexical_analyser)  # <C>

    codeGenerator.addTze()
    emptyTze.append(codeGenerator.getNbOfInstructions()-1)

    lexical_analyser.acceptKeyword("loop")

    suiteInstr(lexical_analyser)  # <A>

    codeGenerator.addTra(adresseDebutBoucle)
    lexical_analyser.acceptKeyword("end")

    codeGenerator.arrayOfinstructions[emptyTze.pop(-1)] = 'tze(' + str(codeGenerator.getNbOfInstructions()+1) + ')'

    logger.debug("end of while loop ")


def altern(lexical_analyser):
    emptyTze = []
    emptyTra = []

    logger.debug("parsing if: ")
    lexical_analyser.acceptKeyword("if")

    expression(lexical_analyser) #C
    codeGenerator.addTze()
    emptyTze.append(codeGenerator.getNbOfInstructions()-1)

    lexical_analyser.acceptKeyword("then")
    suiteInstr(lexical_analyser) #A

    if lexical_analyser.isKeyword("else"):
        lexical_analyser.acceptKeyword("else")

        codeGenerator.addTra()
        emptyTra.append(codeGenerator.getNbOfInstructions()-1)

        codeGenerator.arrayOfinstructions[emptyTze.pop(-1)] = 'tze(' + str(codeGenerator.getNbOfInstructions()+1) + ')'

        suiteInstr(lexical_analyser) #B
        codeGenerator.arrayOfinstructions[emptyTra.pop(-1)] = 'tra(' + str(codeGenerator.getNbOfInstructions()+1) + ')'
    else:
        codeGenerator.arrayOfinstructions[emptyTze.pop(-1)] = 'tze(' + str(codeGenerator.getNbOfInstructions()+1) + ')'

    if(not(lexical_analyser.isKeyword("end"))):
        print("next word is not else or end -> error")
    lexical_analyser.acceptKeyword("end")

    logger.debug("end of if")


def retour(lexical_analyser):
    logger.debug("parsing return instruction")
    lexical_analyser.acceptKeyword("return")
    expression(lexical_analyser)
    codeGenerator.addInstruction("retourFonct()")

########################################################################


def main():

    parser = argparse.ArgumentParser(
        description='Do the syntactical analysis of a NNP program.')
    parser.add_argument('inputfile', type=str, nargs=1,
                        help='name of the input source file')
    parser.add_argument('-o', '--outputfile', dest='outputfile', action='store',
                        default="", help='name of the output file (default: stdout)')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s 1.0')
    parser.add_argument('-d', '--debug', action='store_const', const=logging.DEBUG,
                        default=logging.INFO, help='show debugging info on output')
    parser.add_argument('-p', '--pseudo-code', action='store_const', const=True, default=False,
                        help='enables output of pseudo-code instead of assembly code')
    parser.add_argument('--show-ident-table', action='store_true',
                        help='shows the final identifiers table')
    args = parser.parse_args()

    filename = args.inputfile[0]
    f = None
    try:
        f = open(filename, 'r')
    except:
        print("Error: can\'t open input file!")
        return

    outputFilename = args.outputfile

    # create logger
    LOGGING_LEVEL = args.debug
    logger.setLevel(LOGGING_LEVEL)
    ch = logging.StreamHandler()
    ch.setLevel(LOGGING_LEVEL)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if args.pseudo_code:
        True
    else:
        True

    lexical_analyser = analex.LexicalAnalyser()

    lineIndex = 0
    for line in f:
        line = line.rstrip('\r\n')
        lexical_analyser.analyse_line(lineIndex, line)
        lineIndex = lineIndex + 1
    f.close()

    # launch the analysis of the program
    lexical_analyser.init_analyser()
    program(lexical_analyser)

    if args.show_ident_table:
        print("------ IDENTIFIER TABLE ------")
        # print str(identifierTable)
        print("------ END OF IDENTIFIER TABLE ------")

    if outputFilename != "":
        try:
            output_file = open(outputFilename, 'w')
        except:
            print("Error: can\'t open output file!")
            return
    else:
        output_file = sys.stdout

    # Outputs the generated code to a file
    instrIndex = 0
    while instrIndex < codeGenerator.get_instruction_counter():
           output_file.write("%s\n" % str(codeGenerator.get_instruction_at_index(instrIndex)))
           instrIndex += 1

    if outputFilename != "":
        output_file.close()

    # for i in range(len(codeGenerator.arrayOfinstructions)):
    #     print(codeGenerator.arrayOfinstructions[i])

########################################################################


if __name__ == "__main__":
    main()
