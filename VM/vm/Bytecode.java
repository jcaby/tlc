package vm;

public class Bytecode {
    public static final short debutProg=1;
    public static final short reserver=2;
    public static final short empiler=3;
    public static final short affectation=4;
    public static final short valeurPile=5;
    public static final short add=6;
    public static final short diff=7;
    public static final short tze=8;
    public static final short put=9;
    public static final short get=10;
    public static final short tra=11;
    public static final short finProg=12;
    public static final short egal=13;
    public static final short et=14;
    public static final short ou=15;
    public static final short sup=16;
    public static final short mult=17;
    public static final short moins=18;
    public static final short non=19;
    public static final short infeg=20;
    public static final short sous=21;
    public static final short div=22;
    public static final short inf=23;
    public static final short supeg=24;
    public static final short reserverBloc=25;
    public static final short traStat=26;
    public static final short retourProc=27;
    public static final short empilerAd=28;
    public static final short empilerParam=29;
    public static final short retourFonc=30;
}