package vm;

import static vm.Bytecode.debutProg;
import static vm.Bytecode.finProg;
import static vm.Bytecode.empiler;
import static vm.Bytecode.affectation;
import static vm.Bytecode.reserver;
import static vm.Bytecode.put;
import static vm.Bytecode.valeurPile;
import static vm.Bytecode.diff;
import static vm.Bytecode.add;
import static vm.Bytecode.tze;
import static vm.Bytecode.tra;
import static vm.Bytecode.egal;
import static vm.Bytecode.et;
import static vm.Bytecode.ou;
import static vm.Bytecode.sup;
import static vm.Bytecode.mult;
import static vm.Bytecode.moins;
import static vm.Bytecode.non;
import static vm.Bytecode.infeg;
import static vm.Bytecode.sous;
import static vm.Bytecode.div;
import static vm.Bytecode.inf;
import static vm.Bytecode.supeg;
import static vm.Bytecode.get;
import static vm.Bytecode.reserverBloc;
import static vm.Bytecode.traStat;
import static vm.Bytecode.retourProc;
import static vm.Bytecode.empilerAd;
import static vm.Bytecode.empilerParam;
import static vm.Bytecode.retourFonc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ConversionObjectCode {
  public static final int DEFAULT_ARRAY_SIZE = 1000;

  public static int ip = 0;
  static int[] code = new int[DEFAULT_ARRAY_SIZE];

  static ArrayList<Integer[]> LinesForTzeMaj = new ArrayList<Integer[]>();
  static ArrayList<Integer[]> LinesForTraMaj = new ArrayList<Integer[]>();

  static int nextTze = 0;
  static int nextTra = 0;

  /**************************************************************************************************************
  * le but de ces fonctions est d'adapter le code objet produit par anasyn en une version compatible avec la VM *
  **************************************************************************************************************/

  static void addTze(int line) {
    Integer array[] = { line, -1, 0 }; // ligne destination dans l'ancien code objet, l'emplacement dans le nouveau
                                       // code objet, le nombre de lignes supplémentaires dans le nouveau code objet
    LinesForTzeMaj.add(array);
  }

  static void setIpForTze(int ip_) {
    LinesForTzeMaj.get(nextTze)[1] = ip_;
    nextTze++;
  }

  static void addTra(int line) {
    Integer array[] = { line, -1, 0 };
    LinesForTraMaj.add(array);
  }

  static void setIpForTra(int ip_) {
    LinesForTraMaj.get(nextTra)[1] = ip_;
    nextTra++;
  }

  /* permet de compléter le terme des sauts, une fois que le reste du code objet a été produit */
  static void writeJumps(int Nbline) {
    int size = LinesForTzeMaj.size(), i = 0;
    while (i < size) {
      if (LinesForTzeMaj.get(i)[0] == Nbline) { // si la ligne destination du saut est égale à la ligne courante
        code[LinesForTzeMaj.get(i)[1]] = LinesForTzeMaj.get(i)[0] + LinesForTzeMaj.get(i)[2];
        LinesForTzeMaj.remove(i);
        size--;
      } else {
        i++;
      }
    }

    size = LinesForTraMaj.size();
    i = 0;
    while (i < size) {
      if (LinesForTraMaj.get(i)[0] == Nbline) { // si la ligne destination du saut est égale à la ligne courante
        code[LinesForTraMaj.get(i)[1]] = LinesForTraMaj.get(i)[0] + LinesForTraMaj.get(i)[2];
        LinesForTraMaj.remove(i);
        size--;
      } else {
        i++;
      }
    }
  }

  static int getNbLineForTra() {
    Integer res = LinesForTraMaj.get(0)[0] + LinesForTraMaj.get(0)[1];
    System.out.println("LinesForTraMaj.get(0)[0]=" + LinesForTraMaj.get(0)[0] + " LinesForTraMaj.get(0)[1]="
        + LinesForTraMaj.get(0)[1]);
    LinesForTraMaj.remove(0);
    return res;
  }

  static void incrementNbNewLinesForJumps(int line) {
    for (Integer[] array : LinesForTzeMaj) {
      if (array[0] > line) {
        array[2]++;
      }
    }
    for (Integer[] array : LinesForTraMaj) {
      if (array[0] > line) {
        array[2]++;
      }
    }
  }

  /* la plupart des lignes sont traduites directement */
  /* on garde en mémoire le nombre de nouvelles ligne, par rapport au code objet de base */
  static void convertLine(String data, int line) {
    if (data.equals("debutProg")) {
      code[ip++] = debutProg;
    } else if (data.equals("finProg")) {
      code[ip++] = finProg;
    } else if (data.matches("^empiler\\(\\d+\\)$")) {
      int entier = Integer.parseInt(data.replaceFirst("empiler\\(", "").replaceFirst("\\)", ""));
      code[ip++] = empiler;
      code[ip++] = entier;
      incrementNbNewLinesForJumps(line);
    } else if (data.equals("affectation()")) {
      code[ip++] = affectation;
    } else if (data.matches("^reserver\\(\\d+\\)$")) {
      int entier = Integer.parseInt(data.replaceFirst("reserver\\(", "").replaceFirst("\\)", ""));
      code[ip++] = reserver;
      code[ip++] = entier;
      incrementNbNewLinesForJumps(line);
    } else if (data.equals("put()")) {
      code[ip++] = put;
    } else if (data.equals("get()")) {
      code[ip++] = get;
    } else if (data.equals("valeurPile()")) {
      code[ip++] = valeurPile;
    } else if (data.equals("diff()")) {
      code[ip++] = diff;
    } else if (data.equals("add()")) {
      code[ip++] = add;
    } else if (data.equals("sous()")) {
      code[ip++] = sous;
    } else if (data.equals("div()")) {
      code[ip++] = div;
    } else if (data.matches("^tze\\(\\d+\\)$")) {
      code[ip++] = tze;
      setIpForTze(ip);
      code[ip] = -1;
      ip++;
      incrementNbNewLinesForJumps(line);
    } else if (data.matches("^tra\\(\\d+\\)$")) {
      code[ip++] = tra;
      setIpForTra(ip);
      code[ip] = -1;
      ip++;
      incrementNbNewLinesForJumps(line);
    } else if (data.matches("^traStat\\(\\d+\\,\\d+\\)$")) {
      code[ip++] = traStat;
      setIpForTra(ip);
      code[ip] = -1;
      ip++;
      int entier2 = Integer.parseInt(data.replaceFirst("traStat\\(\\d+,", "").replaceFirst("\\)$", ""));
      incrementNbNewLinesForJumps(line);
      incrementNbNewLinesForJumps(line);
      code[ip++] = entier2;
    } else if (data.equals("egal()")) {
      code[ip++] = egal;
    } else if (data.equals("ou()")) {
      code[ip++] = ou;
    } else if (data.equals("et()")) {
      code[ip++] = et;
    } else if (data.equals("sup()")) {
      code[ip++] = sup;
    } else if (data.equals("supeg()")) {
      code[ip++] = supeg;
    } else if (data.equals("mult()")) {
      code[ip++] = mult;
    } else if (data.equals("moins()")) {
      code[ip++] = moins;
    } else if (data.equals("non()")) {
      code[ip++] = non;
    } else if (data.equals("infeg()")) {
      code[ip++] = infeg;
    } else if (data.equals("inf()")) {
      code[ip++] = inf;
    } else if (data.equals("reserverBloc()")) {
      code[ip++] = reserverBloc;
    } else if (data.equals("retourProc()")) {
      code[ip++] = retourProc;
    } else if (data.matches("^empilerAd\\(\\d+\\)$")) {
      int entier = Integer.parseInt(data.replaceFirst("empilerAd\\(", "").replaceFirst("\\)", ""));
      code[ip++] = empilerAd;
      code[ip++] = entier;
      incrementNbNewLinesForJumps(line);
    } else if (data.matches("^empilerParam\\(\\d+\\)$")) {
      int entier = Integer.parseInt(data.replaceFirst("empilerParam\\(", "").replaceFirst("\\)", ""));
      code[ip++] = empilerParam;
      code[ip++] = entier;
      incrementNbNewLinesForJumps(line);
    } else if (data.equals("retourFonct()")) {
      code[ip++] = retourFonc;
    } else {
      throw new Error("invalid data: "+data);
    }
  }

  static void printCode() {
    System.out.println("-----------------");
    for (int i = 0; i <= ip - 1; i++) {
      System.out.println(i+": " +code[i]);
    }
    System.out.println("-----------------");
  }

  static void setUpJumps(File file) {
    try {
      Scanner reader = new Scanner(file);
      while (reader.hasNextLine()) {
        String data = reader.nextLine();
        if (data.matches("^tze\\(\\d+\\)$")) {
          int entier = Integer.parseInt(data.replaceFirst("tze\\(", "").replaceFirst("\\)", "")) - 1;
          addTze(entier);
        } else if (data.matches("^tra\\(\\d+\\)$")) {
          int entier = Integer.parseInt(data.replaceFirst("tra\\(", "").replaceFirst("\\)", "")) - 1;
          addTra(entier);
        } else if (data.matches("^traStat\\(\\d+\\,\\d+\\)$")) {
          int entier = Integer.parseInt(data.replaceFirst("traStat\\(", "").replaceFirst(",\\d+\\)$", "")) - 1;
          addTra(entier);
        }
      }

      reader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }

  static int[] convert(String path) {
    try {
      File file = new File(path);

      /* on identifie les tra et tze en avance */
      setUpJumps(file);

      /* on adapte toutes les autres instructions directement (à part tra et tze) */
      Scanner reader = new Scanner(file);
      int line = 0;
      ip = 0;
      while (reader.hasNextLine()) {
        String data = reader.nextLine();
        convertLine(data, line);
        line++;
      }
      reader.close();

      /* on complètement le terme des sauts une fois toutes les instructions ajoutées */
      for (int i = 0; i < line; i++) {
        writeJumps(i);
      }
      return code;

    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
      return null;
    }

  }

  public static void main(String[] args) {
    convert("./src/output.txt");
  }
}