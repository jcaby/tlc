package vm;

public class Main {
    static int[] codeObjet = ConversionObjectCode.convert("./src/output.txt");

    public static void main(String[] args) {
        VM vm = new VM(codeObjet);
        vm.exec();
    }
}