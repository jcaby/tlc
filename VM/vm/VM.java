package vm;

import java.util.Scanner;

import static vm.Bytecode.debutProg;
import static vm.Bytecode.finProg;
import static vm.Bytecode.empiler;
import static vm.Bytecode.affectation;
import static vm.Bytecode.reserver;
import static vm.Bytecode.put;
import static vm.Bytecode.valeurPile;
import static vm.Bytecode.diff;
import static vm.Bytecode.add;
import static vm.Bytecode.tze;
import static vm.Bytecode.tra;
import static vm.Bytecode.egal;
import static vm.Bytecode.et;
import static vm.Bytecode.ou;
import static vm.Bytecode.sup;
import static vm.Bytecode.mult;
import static vm.Bytecode.moins;
import static vm.Bytecode.non;
import static vm.Bytecode.infeg;
import static vm.Bytecode.sous;
import static vm.Bytecode.div;
import static vm.Bytecode.inf;
import static vm.Bytecode.supeg;
import static vm.Bytecode.get;
import static vm.Bytecode.reserverBloc;
import static vm.Bytecode.traStat;
import static vm.Bytecode.retourProc;
import static vm.Bytecode.empilerAd;
import static vm.Bytecode.empilerParam;
import static vm.Bytecode.retourFonc;

public class VM {

    public static final int DEFAULT_STACK_SIZE = 1000;

    int cp;             // instruction pointer
    int ip = -1;          // stack pointer

    int[] code;         // code memory
    int[] stack;      // pile


    public VM(int[] code) {
        this.code = code;
        stack = new int[DEFAULT_STACK_SIZE];
    }

    public void exec() {
        cp = 0;
        cpu();
    }

    public void afficherStack(String str, int taille){
        System.out.println(str+": print stack ================");
        for(int i = taille; i>=0; i--){
            System.out.println(i+": "+stack[i]);
        }
        System.out.println("==========================");
    }

    protected void cpu() {
        int opcode = code[cp];
        int base = 0;
        int a,b,addr,v;
        while (opcode!= finProg && cp < code.length) {
            cp++; //jump to next instruction
            
            switch (opcode) {
                case debutProg:
                    break;
                case reserver:
                int nbVariables = code[cp++];
                for(int i = 0; i < nbVariables; i++){
                    ip++;
                }
                    break;
                case empiler:
                    stack[++ip]=code[cp++];
                    break;
                case empilerAd:
                    stack[++ip]=code[(cp++)]+2+base;
                    break;
                case empilerParam:
                    stack[++ip]=code[(cp++)]+2+base;
                    addr=stack[ip--];
                    stack[++ip]=stack[addr];
                    break;
                case affectation:
                    v=stack[ip--];
                    addr=stack[ip--];
                    stack[addr]=v;
                    break;
                case valeurPile:
                    addr=stack[ip--];
                    stack[++ip]=stack[addr];
                    break;
                case egal:
                    a=stack[ip--];
                    b=stack[ip--];
                    if(a==b) {stack[++ip]=1;} else {stack[++ip]=0;}                   
                    break;
                case diff:
                    a=stack[ip--];
                    b=stack[ip--];
                    if(a!=b) {stack[++ip]=1;} else {stack[++ip]=0;}
                    break;
                case add:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a+b;
                    break;
                case sous:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=b-a;
                    break;
                case div:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=b/a;
                    break;
                case mult:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a*b;                  
                    break;
                case moins:
                    a=stack[ip--];
                    stack[++ip]=-a;
                    break;
                case put:
                    System.out.println(stack[ip--]);
                    break;
                case get:
                    addr=stack[ip--];
                    System.out.print("Input : ");
                    Scanner sc= new Scanner(System.in);
                    v= sc.nextInt(); 
                    stack[addr]=v; 
                    break;
                case tze:
                    v=stack[ip--];
                    if(v==1) {
                        cp++;
                    } else if(v==0) {
                        cp=code[cp];
                    }
                    break;
                case tra:
                    cp=code[cp];
                    break;
                case et:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a&b; //binary operator
                    break;
                case ou:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a|b; //binary operator 
                    break;
                case non:
                    a=stack[ip--];
                    if(a==1){
                        stack[++ip]=0;
                    } else{
                        stack[++ip]=1;
                    }
                    break;
                case sup:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a>b ? 1 : 0;
                    break;
                case inf:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a<b ? 1 : 0;
                    break;
                case supeg:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a>=b ? 1 : 0;
                    break;
                case infeg:
                    a=stack[ip--];
                    b=stack[ip--];
                    stack[++ip]=a<=b ? 1 : 0;
                    break;
                case reserverBloc:
                    stack[++ip]=base; //sauvegarder la base courante
                    stack[++ip]=-1; //laisser un emplacement pour sauvegarder le co
                    break;
                case traStat:
                    int a_ = code[cp++];
                    int nbp = code[cp++];
                    stack[ip-nbp] = cp; //sauvegarder le co
                    base=ip-nbp-1; //changement de base
                    cp = a_; //saut du co
                    break;
                case retourProc:
                    ip = base; //retourner à l'état de la pile avant l'appel de la fonction         
                    cp = stack[base + 1]; //saut vers instruction après appel
                    base = stack[base]; //restitution de la base précédente
                    break;
                case retourFonc:
                    int res = stack[ip]; //élément à retourner
                    ip = base; //retourner à l'état de la pile avant l'appel de la fonction
                    cp = stack[base + 1]; //saut vers instruction après appel
                    base = stack[base]; //restitution de la base précédente
                    stack[ip]=res; //élément à retourner au somment de la pile
                    break;
                default :
                    throw new Error("invalid opcode: "+opcode+" at cp="+(cp-1));
            }
            opcode = code[cp];
        }
    }
}